set :output, "#{path}/log/cron.log"
every 1.hours do
  command "ruby #{path}/app.rb"
end
