lock "~> 3.11.0"

set :application, "PikabuMe"
set :repo_url, "deploy@185.26.98.204:/var/repos/PikabuMe.git"
set :branch, "master"
set :keep_releases, 3
set :use_sudo, :sudo
set :deploy_to, "/usr/app/PikabuMe"
set :linked_dirs, %w{log}
set :linked_files, %w{.env complete_ids.yml}
set :rvm_map_bins, %w{gem rake ruby bundle rvmsudo}
set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }


namespace :deploy do
  desc 'Setup'
  task :setup do
    on roles(:all) do
      execute ">>  #{shared_path}/complete_ids.yml"
      execute "rm -rf #{shared_path}/log/"
      execute "mkdir  #{shared_path}/log/"
    end
  end

  desc 'Start script'
  task :start do
    on roles(:all) do
      execute "ruby #{current_path}/app.rb"
    end
  end
end

namespace :whenever do
  desc 'Start worker'
  task :start do
    on roles(:all) do
      within release_path do
        execute :whenever, "-i #{fetch(:application)}"
      end
    end
  end

  task :stop do
    on roles(:all) do
      within release_path do
        execute :whenever, "-c #{fetch(:application)}"
      end
    end
  end

  task :update do
    on roles(:all) do
      within release_path do
        execute :whenever, "-i #{fetch(:application)}"
      end
    end
  end

  task :status do
    on roles(:all) do
      within release_path do
        execute :crontab, "-l"
      end
    end
  end

  after :deploy, "whenever:update"
end
