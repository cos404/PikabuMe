require 'nokogiri'
require 'open-uri'
require 'telegram/bot'
require 'yaml'
require_relative './pika_lib'
require 'dotenv'
Dotenv.load("#{File.dirname(__FILE__)}/.env")

TOKEN_TELEGRAM    = ENV['TOKEN_TELEGRAM']
CHANNEL_TELEGRAM  = ENV['CHANNEL_TELEGRAM']
FEED_URL          = ENV['FEED_URL']
FILE_PATH         = File.dirname(__FILE__)

puts Time.now.strftime("New loop on %H:%M %d/%m/%Y")
File.new("#{FILE_PATH}/complete_ids.yml", 'w') unless File.exist?("#{FILE_PATH}/complete_ids.yml")

Telegram::Bot::Client.run(TOKEN_TELEGRAM) do |bot|
  feeds_ids = YAML.load_file("#{FILE_PATH}/complete_ids.yml") || []
  html = Nokogiri::HTML(open(FEED_URL))
  html.encoding = 'utf-8'
  posts = get_posts(html)

  posts.each do |post|
    title = get_post_title(post)
    link = get_post_link(post)
    id = get_post_id(link)
    images = get_post_images(post)
    text = get_post_text(post)
    tags = get_post_tags(post)
    link_title = "Открыть пост"

    if feeds_ids.include?(id)
      next
    else
      puts link
      feeds_ids << id
    end

    if images.count == 1 && text.join(" ").length < 10
      puts 1
      open(images[0]) { |image|
        File.open("image.jpg","w+") { |file| file.write(image.read) }
      }

      full_length = title.length + link.length + link_title.length
      if full_length > 190
        max_title_length = 190 - link_title.length - link.length - 3
        title = title[0..190-max_title_length] + "..."
      end

      bot.api.send_photo(
        chat_id: CHANNEL_TELEGRAM,
        photo: Faraday::UploadIO.new('./image.jpg', 'image/jpeg'),
        caption: "#{title}\n\n[#{link_title}](#{link})",
        parse_mode: "Markdown"
      )
    elsif text.join("\n").length < 50
      puts 2
      bot.api.sendMessage(
        chat_id: CHANNEL_TELEGRAM,
        text: "#{tags}\n\n[#{link_title}](#{link})",
        parse_mode: "Markdown"
      )
    elsif images.nil? && text.join("\n\n").length < 800
      puts 3
      bot.api.sendMessage(
        chat_id: CHANNEL_TELEGRAM,
        text: "#{text.join("\n\n")}\n\n#{tags}\n\n[#{link_title}](#{link})",
        parse_mode: "Markdown",
        disable_web_page_preview: true
      )
    elsif text[0].length < 50 && text[1].length > 50
      puts 4
      bot.api.sendMessage(
        chat_id: CHANNEL_TELEGRAM,
        text: "#{text[0]}\n#{text[1]}...\n\n#{tags}\n\n[#{link_title}](#{link})",
        parse_mode: "Markdown"
      )
    else
      puts 5
      bot.api.sendMessage(
        chat_id: CHANNEL_TELEGRAM,
        text: "#{text[0]}\n\n#{tags}\n\n[#{link_title}](#{link})",
        parse_mode: 'Markdown'
      )
    end

    sleep 5
  end

  File.open("#{FILE_PATH}/complete_ids.yml", 'w+') do |f|
    f.write(feeds_ids.to_yaml)
  end
end
