def get_posts(html)
  posts = html.xpath('//div[@class="stories-feed__container"]/article')
  posts.pop
  return posts
end

def get_post_title(post)
  post.xpath(".//a[@class='story__title-link']/text()").to_s
end

def get_post_link(post)
  post.xpath(".//a[@class='story__title-link']/@href").to_s
end

def get_post_id(url)
  url.match(/\d*$/).to_s
end

def get_post_raiting(post)
end

def get_post_tags(post)
  post
  .xpath(".//a[@class='tags__tag']/text()")
  .map(&:to_s)
  .map{|e| "#" + e.gsub(/\p{P}|\s/, '\_').downcase }
  .join(" ")
end

def unrated?(tags)
  tags.include?("#Без_рейтинга")
end

def get_post_images(post)
  post
  .xpath(".//div[@class='story-image__content']/a/img/@*[name()='src' or name()='data-src']")
  .map(&:to_s)
end

def get_post_text(post)
  post
  .xpath(".//div[@class='story-block story-block_type_text']//p/text()")
  .map{|paragraph| paragraph.to_s.gsub(/(\*|\_|\`)/, '')}
  .reject(&:empty?)
end
